import { TestBed } from '@angular/core/testing';

import { MedicalDevicesDataService } from './medical-devices-data.service';

describe('MedicalDevicesDataService', () => {
  let service: MedicalDevicesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MedicalDevicesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
