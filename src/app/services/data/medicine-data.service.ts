import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Medicine } from 'src/app/search-menu/search-result/medicine/medicine.component';


@Injectable({
  providedIn: 'root'
})
export class MedicineDataService {

  medicineSearchUserOption!: string;
  constructor(private http: HttpClient) { }


  retriveSearchMedicines(searchMedicine: string) {
    return this.http.get<Medicine[]>(`http://localhost:8082/api/search?medicine=${this.medicineSearchUserOption}`);
  }

}
