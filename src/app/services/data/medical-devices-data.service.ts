import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MedicalDevice, MedicalDeviceComponent } from 'src/app/search-menu/search-result/medical-device/medical-device.component';

@Injectable({
  providedIn: 'root'
})
export class MedicalDevicesDataService {

  medicalDevicesSearchUserOption!: string;

  constructor(private http: HttpClient) { }

  retrieveMedicalDeviceData(searchMedicalDeviec: string) {
    return this.http.get<MedicalDevice[]>(`http://localhost:8082/api/search_medical_device?medicalDevice=${this.medicalDevicesSearchUserOption}`);
  }
}
