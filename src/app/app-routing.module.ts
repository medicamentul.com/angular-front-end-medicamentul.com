import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from './header/login/forgot-password/forgot-password.component';
import { LoginComponent } from './header/login/login.component';
import { SignupComponent } from './header/login/signup/signup.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MedicalDeviceComponent } from './search-menu/search-result/medical-device/medical-device.component';
import { MedicineComponent } from './search-menu/search-result/medicine/medicine.component';

const appRoutes: Routes = [
  { path: '', component: HomePageComponent },
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'medicine-search-result', component: MedicineComponent },
  { path: 'medical-device-search-result', component: MedicalDeviceComponent },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found' }, // This is the last route in the app, don't forget!!!
  { path: '', redirectTo: '/somewhere-else', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
