import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { MapComponent } from './map/map.component';
import { SearchMenuComponent } from './search-menu/search-menu.component';
import { LoginComponent } from './header/login/login.component';
import { SignupComponent } from './header/login/signup/signup.component'
import { ForgotPasswordComponent } from './header/login/forgot-password/forgot-password.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MedicineComponent } from './search-menu/search-result/medicine/medicine.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { APP_BASE_HREF } from '@angular/common';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';

import {
  faTwitter,
  faFacebookF,
  faInstagram,
  faYoutubeSquare,
  faLinkedin,
  fab,
} from '@fortawesome/free-brands-svg-icons';
import { MedicalDeviceComponent } from './search-menu/search-result/medical-device/medical-device.component';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    MapComponent,
    SearchMenuComponent,
    LoginComponent,
    SignupComponent,
    HomePageComponent,
    ForgotPasswordComponent,
    PageNotFoundComponent,
    MedicineComponent,
    MedicalDeviceComponent,
    // AppRoutingModule

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [{
    provide: APP_BASE_HREF, useValue: '/'
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faTwitter, faFacebookF, faLinkedin, faYoutubeSquare, faInstagram);
  }
}
