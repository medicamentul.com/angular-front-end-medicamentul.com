import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  genders = ['masculin', 'feminin'];
  defaultGender = 'masculin';
  signupForm!: FormGroup ;

  constructor() { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      //FormControl() takes 3 param, 1- the initial value, 2- the validator/s, 3- potentional asyncronise validators
      'username': new FormControl(null, [
        Validators.required, 
        Validators.maxLength(35),
        Validators.minLength(2)
    ]),
      'lastname': new FormControl(null, [
        Validators.required,
        Validators.maxLength(35),
        Validators.minLength(2)
      ]),
      'firstname': new FormControl(null, [
        Validators.required,
        Validators.maxLength(35),
        Validators.minLength(2)
      ]),
      'age': new FormControl(null, [
        Validators.min(8),
        Validators.max(110)
      ]),
      'gender': new FormControl('masculin'),
      'email': new FormControl(null,[
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(100)
      ]),

    });
  }

  onSubmit() {
    console.log(this.signupForm);
  }


  
}
