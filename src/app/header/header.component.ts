import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { 
  faFacebookF, 
  faTwitter,
  faInstagram,
  faYoutubeSquare,
  faLinkedinIn,
 } from '@fortawesome/free-brands-svg-icons';
 import {
  faSignInAlt
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faInstagram = faInstagram;
  faYoutubeSquare = faYoutubeSquare;
  faLinkedinIn = faLinkedinIn;
  faSignInAlt = faSignInAlt;
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onLoadLogin() {
    this.router.navigate(['/login']);
  }

}
