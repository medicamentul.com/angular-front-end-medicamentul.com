import { ComponentFactoryResolver } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
import { Component } from '@angular/core';
// import { fa-facebook-f } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss',
    './app.component.home-layout.scss'
  ]
})
export class AppComponent {
  title = 'ng-medicamentul';
 
}
