export enum MenuButtons {

    INSTRUMENTE_MEDICALE = "Instrumente medicale...",
    MEDICAMENTE_OTX_RX = "Medicamente cu prescriptie si fara prescriptie ( OTC & Rx ) ...",
    CLASIFICARE_BOLI = "Clasificarea bolilor ( ICD - 10 ) ..."

}
