import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  faFlask,
  faPlus,
  faMortarPestle,
  faHandHoldingMedical,
  faHeart,
  faStethoscope,
  faCapsules,
  faStarOfLife,
} from '@fortawesome/free-solid-svg-icons';
import { MedicalDevicesDataService } from '../services/data/medical-devices-data.service';
import { MedicineDataService } from '../services/data/medicine-data.service';
import { MenuButtons } from './enum';

@Component({
  selector: 'app-search-menu',
  templateUrl: './search-menu.component.html',
  styleUrls: ['./search-menu.component.scss']
})
export class SearchMenuComponent implements OnInit {

  faFlask = faFlask;
  faPlus = faPlus;
  faMortarPestle = faMortarPestle;
  faHandHoldingMedical = faHandHoldingMedical;
  faHeart = faHeart;
  faStethoscope = faStethoscope;
  faCapsules = faCapsules;
  faStarOfLife = faStarOfLife;

  instrumenteMedicale: MenuButtons = MenuButtons.INSTRUMENTE_MEDICALE;
  medicamenteOtcRx: MenuButtons = MenuButtons.MEDICAMENTE_OTX_RX;
  clasificareBoli: MenuButtons = MenuButtons.CLASIFICARE_BOLI;

  menuButtons: MenuButtons = this.medicamenteOtcRx;

  searchQuery: string = "";
  medicineSearchForm!: FormGroup;

  constructor(
    private medicineDataService: MedicineDataService,
    private router: Router,
    private route: ActivatedRoute,
    private medicalDevicesDataService: MedicalDevicesDataService
  ) { }

  ngOnInit(): void {
  }

  setCurrentSearch(menuButton: MenuButtons) {
    this.menuButtons = menuButton;
  }

  onSelectedDataBase() {
    if (this.menuButtons === this.medicamenteOtcRx) {
      this.onMedicineSearchUserOption();
    } else if (this.menuButtons === this.instrumenteMedicale) {
      this.onMedicalDeviceSearchUserOption()
    } else this.onDiseaseClasificationOption()
  }

  onMedicineSearchUserOption() {
    console.log('Medicine Search');
    if (this.searchQuery != "" && this.searchQuery.length < 300 &&
      this.searchQuery.trim().length != 0) {
      this.medicineDataService.medicineSearchUserOption = this.searchQuery.trim();
      this.router.navigate(['medicine-search-result'])
      //{ relativeTo: this.route }); - delete this because we dont have, face append la noua 
    }
  }

  onMedicalDeviceSearchUserOption() {
    console.log('Medical Devices Search');
    if (this.searchQuery != "" && this.searchQuery.length < 300 &&
      this.searchQuery.trim().length != 0) {
      this.medicalDevicesDataService.medicalDevicesSearchUserOption = this.searchQuery.trim();
      this.router.navigate(['medical-device-search-result'])
      //{ relativeTo: this.route }); - delete this because we dont have, face append la noua 
    }
  }

  onDiseaseClasificationOption() {
    return null;
    //{ relativeTo: this.route }); - delete this because we dont have, face append la noua 
  }

}





// <div class="menu box grid" >
//   <div class="sleek__menu__buttons" >
//     <a href="#" class="btn instruments"(click) = "setCurrentSearch(instrumenteMedicale)"
//     [class.btn__sleek--active]="menuButtons === instrumenteMedicale" >
//       <!-- < fa - icon class="fas"[icon] = "faStethoscope" > </fa-icon> -->
//         < i class="fas fa-stethoscope" >
//           <p>Instrumente < br > Medicale < /p>
//           < /i>
//           < /a>
//           < a href = "#" class="btn medicines"(click) = "setCurrentSearch(medicamenteOtcRx)"
//           [class.btn__sleek--active]="menuButtons === medicamenteOtcRx" >
//             <i class="fas fa-capsules" >
//               <!-- < fa - icon class="fas"[icon] = "faCapsules" > </fa-icon> -->
//                 < br >
//                 <p>Medicamente < br > OTC & <br>Rx < /p>
//                 < /i>
//                 < /a>
//                 < a href = "#" class="btn icd-10"(click) = "setCurrentSearch(clasificareBoli)"
//                 [class.btn__sleek--active]="menuButtons === clasificareBoli" >
//                   <i class="fas fa-star-of-life" >
//                     <!-- < fa - icon class="fas"[icon] = "faStarOfLife" > </fa-icon> -->
//                       < br >
//                       <p>Clasificarea < br > Bolilor < br > ICD - 10 < /p>
//                       < /i>
//                       < /a>
//                       < /div>
//                       < /div>