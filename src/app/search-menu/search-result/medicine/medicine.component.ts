import { Component, OnInit } from '@angular/core';
import { MedicineDataService } from 'src/app/services/data/medicine-data.service';

export class Medicine {
  
  constructor(
    public codIdentificareMedicament: String,
    public denumireComerciala: string,
    public denumireComercialaInternationala: string,
    public formaFarmaceutica: string,
    public concentratie: string,
    public firmaTaraProducatoare: string,
    public firmaTaraDetinatoare: string,
    public clasificareAnatomicaTerapeuticaChimica: string,
    public actiuneTerapeutica: string,
    public prescriptie: string,
    public dataAmbalaj: string,
    public ambalaj: string,
    public volumAmbalaj: string,
    public valabilitateAmbalaj: string,
    public bulina: string,
    public diez: string,
    public stea: string,
    public triunghi: string,
    public dreptunghi: string,
    public dataActualizare: string
  ) { }

}
@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.component.html',
  styleUrls: ['./medicine.component.scss']
})
export class MedicineComponent implements OnInit {

  medicines!: Medicine[];

  constructor(private medicineDataService: MedicineDataService) { }

  ngOnInit(): void {
    this.medicineDataService.retriveSearchMedicines(this.medicineDataService.medicineSearchUserOption).subscribe(
      response => {
        console.log(response);
        this.medicines = response;
      }
    )
  }
}
