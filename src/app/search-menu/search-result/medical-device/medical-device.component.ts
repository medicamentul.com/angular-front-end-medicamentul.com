import { Component, OnInit } from '@angular/core';
import { MedicalDevicesDataService } from 'src/app/services/data/medical-devices-data.service';

export class MedicalDevice {

  constructor(
   public dispozitivId: string,
    public denumireDispozitiv: string,
    public  tip: string,
    public  clasa: string,
    public  codInregistrare: string,
    public  data: string,
    public  denumireFirmaProducatoare: string,
    public  denumireFirmaReprezentantiAutorizati: string
  ) { }

}
@Component({
  selector: 'app-medical-device',
  templateUrl: './medical-device.component.html',
  styleUrls: ['./medical-device.component.scss']
})
export class MedicalDeviceComponent implements OnInit {


  medicalDevices!: MedicalDevice[];

  constructor(private medicalDeviceService: MedicalDevicesDataService) { }

  ngOnInit(): void {
    this.refreshMedicalDevice();
  }

  refreshMedicalDevice() {
    this.medicalDeviceService.retrieveMedicalDeviceData(this.medicalDeviceService.medicalDevicesSearchUserOption).subscribe(
      response => {
        console.log(response);
        this.medicalDevices = response;
      }
    )
  }

}
