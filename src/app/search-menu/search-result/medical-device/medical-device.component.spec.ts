import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalDeviceComponent } from './medical-device.component';

describe('MedicalDeviceComponent', () => {
  let component: MedicalDeviceComponent;
  let fixture: ComponentFixture<MedicalDeviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalDeviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
